<?php


namespace App\Http\Controllers;

use App\Car;

class CarsController
{
	public function cars()
    {
        $cars = Car::all();
        return view('cars.cars', compact('cars'));
    }

    public function cardetail($car_id)
    {
         $car = Car::findOrFail($car_id);
        return view('cars.cardetail', compact('car'));
    }
}